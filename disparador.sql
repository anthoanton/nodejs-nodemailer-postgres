-- CREATE A TABLE
CREATE TABLE EMAIL_TEST (
   ID SERIAL PRIMARY KEY NOT NULL,
   DESTINO       VARCHAR(50),
   ASUNTO       VARCHAR(50),
   MENSAJE    TEXT NOT NULL
);

-- CREATE A FUNCTION TRIGGER
CREATE OR REPLACE FUNCTION notify_trigger() RETURNS trigger AS $$
DECLARE
BEGIN
  PERFORM pg_notify('watchers', 
    '{' ||
      '"table":"'    || TG_TABLE_NAME || '",' ||
      '"operation":"'|| TG_OP         || '",' ||
      '"row":'       || (select row_to_json(row)::varchar from (SELECT NEW.*) row) ||
    '}'
  );
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- CREATE A TRIGGER
CREATE TRIGGER watched_table_trigger AFTER INSERT ON 
  -- TABLE
  EMAIL_TEST
FOR EACH ROW EXECUTE PROCEDURE notify_trigger();

insert into EMAIL_TEST (destino,asunto, mensaje) values ('anthonyanton01@gmail.com', 'probando mensaje','testeando este mensaje')
