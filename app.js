const nodemailer = require('nodemailer')
const { Client } = require('pg')

const client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'tu-base-datos',
  password: '123456',
  port: 5432,
})
client.connect()

client.query('LISTEN watchers', (err, res) => {
    console.log('Conectado a postgresql')
})


client.on('notification', (msg)=> {
    var data = JSON.parse(msg.payload);

    respuesta = mailer(data.row);

    respuesta.then(async (success)=>{
        console.log('mensaje enviado con exito', success)
    },async (error)=>{
        console.log('mensaje no pudo ser enviado', error)                            
    })

})

var mailer = async (data) =>{

    return new Promise((resolve,reject)=>{
        
      var transporter = nodemailer.createTransport({
        host: "gmail",
        auth: {
          user: "tu gmail",
          pass: "tu contraseña"
        }
      });

      transporter.verify(function(error, success) {
        if (error) {
          console.log(error);
        } else {
          console.log("Server listo para enviar mensajes");
        }
      });

      // Definimos el email
      var mailOptions = {};
          mailOptions.from = "'GeekSystSupport'";
          mailOptions.to = data.destino;
          mailOptions.subject = data.asunto;
          mailOptions.html = data.mensaje;

    console.log("Enviando Correo a "+ data.destino+" asunto: "+data.asunto);
      // Enviamos el email
      transporter.sendMail(mailOptions, function(error, info){
        if (error){
          return reject(error.message)
        }else{
          return resolve(info)
        }
        
          
      });
    })
  }
